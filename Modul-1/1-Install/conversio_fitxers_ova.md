### Conversions entre fitxers plantilla de màquines virtuals

##### Situació Inicial

Ens proporcionen un fitxer d'extensió *ova* (*Linkat.ova*) com a plantilla de màquina virtual.
La idea seria fer servir programari lliure per virtualitzar. Com que *VirtualBox*
és una solució privativa intentarem fer el mateix amb `kvm` que és una solució lliure.

El nostre objectiu inicial és transformar el fitxer `*.ova` en un fitxer
d'extensió `*.qcow2`.  Utilitzarem [aquest
enllaç](http://ask.xmodulo.com/convert-ova-to-qcow2-linux.html)

##### Conversió a format *qcow2*

El primer pas serà extreure la imatge de disk continguda al fitxer `Linkat.ova`. Utilitzarem l'ordre `tar`

```
pingui@localhost linkat-curs]$ tar -xvf Linkat.ova
Linkat.ovf
Linkat-disk001.vmdk
```

Això ens extreu dos fitxers:

* Un fitxer de format *xml*, `Linkat.ovf`, que conté la informació de com s'ha empaquetat el fitxer virtual *ova*.
* Un fitxer, `Linkat-disk001.vmdk`, que és una imatge del disc. Alerta: hi ha diferents versions.

Ara necessitem convertir la imatge de disk de format *vmdk* a *qcow2*. Per fer això podrem fer servir `qemu-img`.

Si es necessita instal·lar, en distribucions *Fedora/RedHat/Centos* (branca *rpm*) podem fer servir `yum/dnf`:

```
dnf install qemu-img
```

En distros GNU/Linux amb gestors de paquets *deb* podem fer servir:

```
apt-get install qemu-img
```

Pots ser prudent mirar abans si la nostre ordre suporta el format de disc que tenim. Això ho podrem fer amb l'ordre:

```
qemu-img -h | tail -n1
Supported formats: null-co iscsi nfs vpc bochs quorum host_cdrom luks raw host_device vhdx blkverify file blkreplay vvfat ssh qcow2 blkdebug tftp ftp ftps https dmg http qed nbd cloop qcow vmdk rbd sheepdog vdi parallels gluster null-aio
```

I veiem que soporta *vmdk* (i també *qcow2*). Per tant, estem en disposició de fer la conversió:

```
qemu-img convert -O qcow2 Linkat-disk001.vmdk Linkat.qcow2
```

Ara ja podem fer executar:

```
virt-manager
```

i utilitzar l'opció "import existing disk image":

![captura de pantalla "creació màquina virtual"](virt-manager-pas1.png)

Finalment linkat ens donarà la benvinguda:

![captura de pantalla del gestor de sessions per defecte de Linkat](linkat_session_manager.png)


##### Links extres

Pot ser d'interés [instal·lació de kvm](instal_kvm.md)


##### Troubleshooting

La plantilla de Virtual Box proporcionada intenta facilitar algunes tasques d'instal·lació per al paquet *guest additions* al client del VirtualBox. Això provoca la sortida d'un missatge flotant quan ens loguegem:

![missatge d'error de VirtualBox](notification.png)

Desconec si és necessari desintal·lar el paquet `unity-scope-virtualbox`, únic paquet de virtualbox que es trobava a la base de dades de paquets *deb*, però el que segur que s'ha de desintal·lar es troba al directori `/opt/VBoxGuestAdditions-5.2.4/`.

Afortunadament existeix un script de desinstal·lació de nom `uninstall.sh` dintre d'aquest directori. De manera que podem executar-lo com a root (o amb sudo):

```
/opt/VBoxGuestAdditions-5.2.4/uninstall.sh
```

Un cop fet això ja no surt més aquest missatge d'error.


