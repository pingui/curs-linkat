### Creació d'usuaris, grups, permissos 


Fem un cop d'ull als grups que hi ha al sistema

```
professor@Linkat:~$ cat /etc/group | grep super
...
sudo:x:27:super
...
```

Creem un user0 (que serà l'usuari que podrà fer sudo) 

```
root@Linkat:~# adduser user0
S'està afegint l'usuari «user0»…
S'està afegint el grup nou user0 (1003)…
S'està afegint el nou usuari user0 (1003) amb grup user0…
S'està creant el directori personal «/home/user0»…
S'estan copiant els fitxers des de «/etc/skel»…
Introduïu la nova contrasenya d'UNIX: 
Torneu a escriure la nova contrasenya d'UNIX: 
passwd: s'ha actualitzat la contrasenya satisfactòriament
S'està canviant la informació d'usuari per a user0
Introduïu el nou valor, o premeu INTRO per al predeterminat
	Nom complet []: 
	Número d'espai []: 
	Telèfon de la feina []: 
	Telèfon de casa []: 
	Altre []: 
És aquesta informació correcta? [S/n] 
```

Fem el mateix per a la resta (user1, user2, user3):

Per afegir usuaris a un grup es pot fer de diferents maneres, a banda de gràficament o afegint directament al fitxer `/etc/group` i amb `usermod` també amb la següent ordre que afegeix l'usuari user0 al grup sudo:

```
root@Linkat:~# gpasswd -a user0 sudo
S'està afegint l'usuari user0 al grup sudo
```

Creem els grups group01 i el grup group23:

```
root@Linkat:~# addgroup group01
S'està afegint el grup 'group01' (GID 1004)…
Fet.
root@Linkat:~# addgroup group23
S'està afegint el grup 'group23' (GID 1005)…
Fet.
```

Afegim l'usuari user0 i user1 al grup group01 i anàlogament user2 i user3 al grup group23

```
root@Linkat:~# gpasswd -a user0 group01 
S'està afegint l'usuari user0 al grup group01
root@Linkat:~# gpasswd -a user1 group01 
S'està afegint l'usuari user1 al grup group01
root@Linkat:~# gpasswd -a user2 group23 
S'està afegint l'usuari user2 al grup group23
root@Linkat:~# gpasswd -a user3 group23 
S'està afegint l'usuari user3 al grup group23
```


Per al directori podem fer moltes coses. Suposarem que el directori es propietat del grup que se'ns demana que tinfui més poder (group01). Comencem canviant a l'usuari user0:

```
professor@Linkat:~$ su -l user0
Contrasenya: 
user0@Linkat:~$ mkdir directori
user0@Linkat:~$ ls -ld directori
drwxrwxr-x 2 user0 user0 4096 abr 14 21:46 directori
```

Canviem el grup propietari del directori:

```
sudo chown :group01 directori
user0@Linkat:~$ ls -ld directori
drwxrwxr-x 2 user0 group01 4096 abr 14 21:46 directori
```

Si ens fixem els permissos de la resta que són "r-x", la "x" permet accedir a aquest directori (fer `cd`) i la "r" llegir el contingut del directori (fer `ls`, o sigui llistar).

Per exemple:

```
user2@Linkat:~$ touch /home/user0/directori/fitxer_nou.txt
touch: no s’han pogut canviar les dates de '/home/user0/directori/fitxer_nou.txt': S’ha denegat el permís
user2@Linkat:~$ ls -l /home/user0/directori/
total 0
-rw-rw-r-- 1 user0 user0 0 abr 14 21:46 a
user2@Linkat:~$ cd /home/user0/directori/
user2@Linkat:/home/user0/directori$
```
